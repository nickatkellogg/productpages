from PIL import Image
try:
    import fnmatch
except Exception:
    import glob
import logging
import math
import os
import sys

logging.basicConfig(level=logging.DEBUG)

def find_files(path, filename_pattern):
    if sys.version_info[0] == 2:
        matches = []
        for root, folder, filenames in os.walk(path):
            for filename in fnmatch.filter(filenames, filename_pattern):
                matches.append(os.path.join(root, filename))
        return sorted(matches)
    elif sys.version_info[1] == 3:
        search_path = path + '/**/' + filename_pattern
        return sorted(list(glob.iglob(search_path, recursive=True)))
    else:
        # Not sure how we'd get here
        raise

def resize_image_fill_canvas(input_filename, target_size, output_path='resized', output_filename=None, fill_color=None):
    """
    input_filename: full path to the input image file
    output_path: This is a subdirectory of the location where the original input_filename is found. If this
                 doesn't exist, it will be created.
    output_filename: full path to the output image file. If None, then defaults to the input_filename path
                     with the target_size added in to the filename base. For example, if input_filename is
                     "c:/input.jpg" and the target_size is (500, 300) then the output_filename will be
                     constructed as "c:/input_500x300.jpg".
    target_size: 2-tuple containing the width and height of the target image size.
    fill_color: tuple matching the mode of the input_filename containing the color code of the fill color
                for the canvas where it needs to be expanded beyond the resized image. If None, then this
                defaults to white ((255,) for L mode, (255, 255, 255) for RGB mode, and (255, 255, 255, 255)
                for RGBA or CMYK mode.).
    """
    path, filename = os.path.split(input_filename)
    filename_base, ext = os.path.splitext(filename)
    logging.debug("Path to input image file: " + path)
    logging.debug("Base of image filename: " + filename_base)
    logging.debug("Filename extension: " + ext)

    target_width, target_height = target_size
    
    if not output_filename:
        new_image_filename = filename_base + "_" + str(target_width) + 'x' + str(target_height) + ext
        output_path = os.path.join(path, output_path)
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        output_filename = os.path.join(output_path, new_image_filename)
        logging.debug("Output filename: " + new_image_filename)
        logging.debug("Output filename (full path): " + output_filename)
    
    input_image = Image.open(input_filename)
    input_image.load()
    image_width, image_height = input_image.size
    input_image.thumbnail((target_width, target_height))
    logging.debug("Target image size: " + str(target_size))
    logging.debug("Input image size: " + str(input_image.size))
    logging.debug("Resized input image size: " + str(input_image.size))
    
    logging.debug("Input image mode: " + str(input_image.mode))
    if not fill_color:
        if len(input_image.mode) == 1: # L, 1
            fill_color = (255,)
        if len(input_image.mode) == 3: # RGB
            fill_color = (255, 255, 255)
        if len(input_image.mode) == 4: # RGBA, CMYK
            fill_color = (255, 255, 255, 255)

    output_image = Image.new(input_image.mode, (target_width, target_height), fill_color)
    logging.debug("Mode for input image: " + str(input_image.mode))
    logging.debug("Mode for output image: " + str(output_image.mode))
    image_width, image_height = input_image.size
    x1 = int(math.floor((target_width - image_width) / 2))
    y1 = int(math.floor((target_height - image_height) / 2))
    logging.debug("Pasting resized input image at coordinates: " + str((x1, y1)))
    output_image.paste(input_image, (x1, y1, x1 + image_width, y1 + image_height))
    output_image.save(output_filename)
    logging.info("Saved resized image to " + output_filename)


image_files = find_files('2018-2019 models', '*.jpg') + find_files('2018-2019 models', '*.png')

for image_file in image_files:
    resize_image_fill_canvas(image_file, (750, 500))
